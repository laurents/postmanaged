# postManaged

A self-managed postgresql setup.

`postManaged` stands for both "postgresql Managed" and "Post(after/beyond) managed databases". It is an all-in-one postgresql database deployment system that aims to replace managed instances like AWS RDS and the like.
It allows deploying a postgresql instance with a handful of supporting tools (config optimisation, query analyser, remote point-in-time backup, various optimisers...) from a single CI job. You just need to provide an IP address and SSH key to a fresh VPS and the desired configuration parameters to setup a database server. The machine can be setup manually or via whatever IaaC system you normally rely on.